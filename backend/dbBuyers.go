/*
@File: dbBuyers.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/

package main

import (
	"encoding/json"
	"log"

	"github.com/dgraph-io/dgo/protos/api"
)

func AddBuyers(date string) {
	buyers := GetBuyers()
	for index, _ := range buyers {
		buyers[index].Created_at = date
	}
	response, err := json.Marshal(buyers)
	if err != nil {
		log.Fatal(err)
	}
	dg := NewClient()
	RunMutation(dg, response)
}

func GetAllBuyers() []byte {
	query := `
		{
			buyers(func: has(id)) {
				id
				age
				name
			}
		}
	`
	dg := NewClient()
	response := RunQuery(dg, query)
	buyers, err := json.Marshal(apiResponseToBuyerStruct(response))
	if err != nil {
		log.Fatal(err)
	}
	return buyers
}

func apiResponseToBuyerStruct(res *api.Response) []Buyer {
	var data struct {
		Buyers []Buyer `json:"buyers,omitempty"`
	}
	if err := json.Unmarshal([]byte(string(res.Json)), &data); err != nil {
		log.Fatal(err)
	}
	return data.Buyers
}
