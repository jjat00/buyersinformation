/*
@File: db.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/

package main

import (
	"context"
	"fmt"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

func RunQuery(dg *dgo.Dgraph, query string) *api.Response {
	txn := dg.NewTxn()
	defer txn.Discard(context.Background())
	res, err := txn.Query(context.Background(), query)
	if err != nil {
		fmt.Println(err)
	}
	return res
}

func RunMutation(dg *dgo.Dgraph, mutuation []byte) {
	txn := dg.NewTxn()
	defer txn.Discard(context.Background())
	mu := &api.Mutation{CommitNow: true}
	mu.SetJson = mutuation
	_, err := txn.Mutate(context.Background(), mu)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("SUCCESSFUL MUTATION")
	}
}

func AlterSchema(dg *dgo.Dgraph, schema string) {
	err := dg.Alter(context.Background(), &api.Operation{
		Schema: schema,
	})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("SUCESSFULL ALTER")
	}
}

func NewClient() *dgo.Dgraph {
	conn, err := grpc.Dial("127.0.0.1:9080", grpc.WithInsecure())
	if err != nil {
		fmt.Println(err)
	}
	return dgo.NewDgraphClient(api.NewDgraphClient(conn))
}
