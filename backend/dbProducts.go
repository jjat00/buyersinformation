/*
@File: dbProducts.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/
package main

import (
	"encoding/json"
	"log"

	"github.com/dgraph-io/dgo/protos/api"
)

func AddProducts(date string) {
	products := GetProducts()
	for index, _ := range products {
		products[index].Created_at = date
	}
	response, err := json.Marshal(products)
	if err != nil {
		log.Fatal(err)
	}
	dg := NewClient()
	RunMutation(dg, response)
}

func ApiResponeToProductStruct(res *api.Response) []Product {
	var data struct {
		Products []Product `json:"products,omitempty"`
	}
	if err := json.Unmarshal([]byte(string(res.Json)), &data); err != nil {
		log.Fatal(err)
	}
	return data.Products
}
