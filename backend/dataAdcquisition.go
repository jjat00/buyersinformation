/*
@File: dataAdcquistion.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

type Buyer struct {
	BuyerId    string `json:"id,omitempty"`
	Name       string `json:"name,omitempty"`
	Age        int    `json:"age,omitempty"`
	Created_at string `json:"created_at,omitempty"`
}

type Transaction struct {
	TransactionId         string   `json:"transactionId,omitempty"`
	BuyerId               string   `json:"buyerId,omitempty"`
	IpAddress             string   `json:"ipAddress,omitempty"`
	Device                string   `json:"device,omitempty"`
	PurchasedProductosIds []string `json:"purchasedProductosIds,omitempty"`
	Created_at            string   `json:"created_at,omitempty"`
}

type Product struct {
	ProductId  string `json:"productId,omitempty"`
	Name       string `json:"name,omitempty"`
	Price      int    `json:"price,omitempty"`
	Created_at string `json:"created_at,omitempty"`
}

func GetBuyers() []Buyer {
	response := getData("https://kqxty15mpg.execute-api.us-east-1.amazonaws.com/buyers")
	var buyers []Buyer
	json.Unmarshal(response, &buyers)
	return buyers
}

func GetProducts() []Product {
	response := getData("https://kqxty15mpg.execute-api.us-east-1.amazonaws.com/products")
	fixNames := strings.ReplaceAll(string(response), "'s", "")
	splitNewLine := strings.Split(fixNames, "\n")
	var products []Product
	for i := 0; i < len(splitNewLine)-1; i++ {
		idNamePrice := strings.Split(splitNewLine[i], "'")
		price, _ := strconv.Atoi(idNamePrice[2])
		products = append(products, Product{ProductId: idNamePrice[0], Name: idNamePrice[1], Price: price})
	}
	return products
}

func GetTransactions() []Transaction {
	response := getData("https://kqxty15mpg.execute-api.us-east-1.amazonaws.com/transactions")
	split := strings.Split(string(response), "#")
	var transactions []Transaction
	for _, each := range split {
		re := regexp.MustCompile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}`)
		ipAddress := re.FindString(each)
		if re.MatchString(each) {
			idsDeviceProductIds := strings.Split(each, ipAddress)
			transactionID := idsDeviceProductIds[0][0:12]
			buyerID := cleanText(idsDeviceProductIds[0][12:])
			deviceAndProducts := strings.Split(idsDeviceProductIds[1], "(")
			device := cleanText(deviceAndProducts[0])
			productIDs := strings.Split(strings.ReplaceAll(deviceAndProducts[1], ")", ""), ",")
			productIDs = cleanArrayText(productIDs)
			transactions = append(transactions, Transaction{TransactionId: transactionID, BuyerId: buyerID, IpAddress: ipAddress, Device: device, PurchasedProductosIds: productIDs})
		}
	}
	return transactions
}

func getData(url string) []byte {
	res, err := http.Get(url)
	if err != nil {
		fmt.Print(err)
	}
	response, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
	}
	return response
}

func cleanText(text string) string {
	newtext := strings.Trim(text, "\x00")
	return newtext
}

func cleanArrayText(text []string) []string {
	var newArrayText []string
	for _, each := range text {
		newArrayText = append(newArrayText, cleanText(each))
	}
	return newArrayText
}
