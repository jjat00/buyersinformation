/*
@File: dbConsultBuyer.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/

package main

import (
	"encoding/json"
	"log"
)

type buyerInformation struct {
	PurchaseHistory []Product `json:"purchaseHistory,omitempty"`
	OtherBuyers     []Buyer   `json:"otherBuyers,omitempty"`
	Recomendations  []Product `json:"recomendations,omitempty"`
}

func GetBuyerInfomration(buyerId string) []byte {
	var buyerInformation buyerInformation
	buyerInformation.PurchaseHistory = getPurchaseHistory(buyerId)
	buyerInformation.OtherBuyers = getBuyersSameIP(buyerId)
	buyerInformation.Recomendations = getRecommendations(buyerId)
	out, err := json.Marshal(buyerInformation)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func getPurchaseHistory(buyerId string) []Product {
	dg := NewClient()
	transactions := getBuyerTransactions(buyerId)
	var purchasedProducts []Product
	for _, eachTransaction := range transactions {
		for _, eachPurchasedProductId := range eachTransaction.PurchasedProductosIds {
			query := `
				{
					products(func: eq(productId,` + eachPurchasedProductId + `)){
						name
						price
          			}
				}			
			`
			response := RunQuery(dg, query)
			product := ApiResponeToProductStruct(response)
			purchasedProducts = append(purchasedProducts, Product{Name: product[0].Name, Price: product[0].Price})
		}
	}
	return purchasedProducts
}

func getBuyersSameIP(buyerId string) []Buyer {
	dg := NewClient()
	transactions := getBuyerTransactions(buyerId)
	var otherBuyers []Buyer
	for _, eachTransaction := range transactions {
		query := `
				{
					transactions(func: eq(ipAddress,` + eachTransaction.IpAddress + `)){
						buyerId
          			}
				}			
			`
		response := RunQuery(dg, query)
		transactions := ApiResponeToTransactionStruct(response)
		for _, eachTransaction := range transactions {
			query = `
				{
					buyers(func: eq(id,` + eachTransaction.BuyerId + `)){
						id
						name
						age
					}
				}			
			`
			response := RunQuery(dg, query)
			buyer := apiResponseToBuyerStruct(response)
			otherBuyers = append(otherBuyers, Buyer{BuyerId: buyer[0].BuyerId, Name: buyer[0].Name, Age: buyer[0].Age})
		}
	}
	return otherBuyers
}

func getRecommendations(buyerId string) []Product {
	otherBuyers := getBuyersSameIP(buyerId)
	var recommendedProducts []Product
	for index := 0; index < 3; index++ {
		buyerProductHistory := getPurchaseHistory(otherBuyers[index].BuyerId)
		for _, eachProduct := range buyerProductHistory {
			recommendedProducts = append(recommendedProducts, eachProduct)
		}
	}
	return recommendedProducts
}

func getBuyerTransactions(buyerId string) []Transaction {
	query := `
		{		
			transactions(func: eq(buyerId,` + buyerId + `)){
				purchasedProductosIds
				ipAddress
			}
		}
	`
	dg := NewClient()
	response := RunQuery(dg, query)
	transactions := ApiResponeToTransactionStruct(response)
	return transactions
}
