/*
@File: dbTransactions.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/
package main

import (
	"encoding/json"
	"log"

	"github.com/dgraph-io/dgo/protos/api"
)

func AddTransactions(date string) {
	transactions := GetTransactions()
	for index, _ := range transactions {
		transactions[index].Created_at = date
	}
	response, err := json.Marshal(transactions)
	if err != nil {
		log.Fatal(err)
	}
	dg := NewClient()
	RunMutation(dg, response)
}

func ApiResponeToTransactionStruct(res *api.Response) []Transaction {
	var data struct {
		Transactions []Transaction `json:"transactions,omitempty"`
	}
	if err := json.Unmarshal([]byte(string(res.Json)), &data); err != nil {
		log.Fatal(err)
	}
	return data.Transactions
}
