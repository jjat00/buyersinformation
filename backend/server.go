/*
@File: server.go
@Author: Jaime Aza
@email: userjjat00@gmail.com
*/
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
)

func router() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/loadData", loadData)
	r.Get("/buyers", listAllBuyers)
	r.Get("/consultBuyer/{buyerId}", consultBuyerInformation)
	return r
}

func LaunchServer() {
	router := router()
	err := http.ListenAndServe(":3000", router)
	if err != nil {
		fmt.Println(err)
	}
}

func loadData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	date := convertToDate(r)
	fmt.Println(date)
	AddBuyers(date)
	AddProducts(date)
	AddTransactions(date)
	//alterSchema()
	fmt.Fprintf(w, string("successfull load"))
}

func listAllBuyers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	buyers := GetAllBuyers()
	fmt.Fprintf(w, string(buyers))
}

func consultBuyerInformation(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	buyerId := string(chi.URLParam(r, "buyerId"))
	buyerInformation := GetBuyerInfomration(buyerId)
	fmt.Fprintf(w, string(buyerInformation))
}

func convertToDate(r *http.Request) string {
	request, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Print(err)
	}
	var timeUnix struct {
		Date string `json:"date,omitempty"`
	}
	err = json.Unmarshal(request, &timeUnix)
	if err != nil {
		fmt.Print(err)
	}
	date := unixTimeToDateString(timeUnix.Date)
	return date
}

func GetDate() string {
	t := time.Now()
	year, mouth, day := t.UTC().Date()
	date := strconv.Itoa(year) + "-" + mouth.String() + "-" + strconv.Itoa(day)
	return date
}

func unixTimeToDateString(unixTime string) string {
	var dateInput string
	if unixTime != "" {
		unix, _ := strconv.ParseInt(unixTime, 10, 64)
		date := time.Unix(unix, 0).UTC().String()
		t, _ := time.Parse("2006-01-02 15:04:05 -0700 MST", date)
		year, mouth, day := t.UTC().Date()
		dateInput = strconv.Itoa(year) + "-" + mouth.String() + "-" + strconv.Itoa(day)
	} else {
		dateInput = GetDate()
	}
	return dateInput
}

func alterSchema() {
	dg := NewClient()
	schema := `
		id: string @index(term) .
		name: string @index(term) .
		created_at: string @index(term) .
		productId: string @index(term) .
		transactionId: string @index(term) .
		ipAddress: string @index(term) .
		buyerId: string @index(term) .
		device: string @index(term) .
	`
	AlterSchema(dg, schema)
}
